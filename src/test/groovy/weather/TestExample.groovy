package weather

import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import spock.lang.Specification
import wssimulator.WSSimulator
import java.util.concurrent.TimeUnit
import static weather.TestingUtils.copyFromTo
import static weather.TestingUtils.waitFor
import static weather.TestingUtils.deleteFiles


@ContextConfiguration(classes = Application.class)
@TestPropertySource(locations = "classpath:test-integration.properties")

class TestExample extends Specification {

    public static final String directoryLocation = "/tmp"

    def "Testing the output.xml file returned"() {
        given: "put the CSV into the directory that CAMEL is listening to"
        copyFromTo("/input/weather/cities.csv", new File(directoryLocation + "/cities.csv"))
        def parser = new XmlSlurper()

        and: "start WSSimulator"
        WSSimulator.setPort(8866)
        WSSimulator.addSimulation(getClass().getResource("/ws/weather/sample-get-weather-response.yml").getFile() as File);
        WSSimulator.shutdown()
        when:
        waitFor(5, TimeUnit.SECONDS) //let camel do its thing.

        then: "read the written values back into this test so we can output them to a print stream (or going further by validating the response)"
        def output = parser.parse(new File(directoryLocation + '/output.xml'))
        //compare String output with the expected response from sample-get-weather-response
        //output.contains("<Location>FAILURE STACKTRACE / Norther Ireland(EGAC) 54-36N 005-53W 0M</Location>")

                println "output:-------\n$output\n-------"

        true
        cleanup: "delete all files"
        deleteFiles(new File(directoryLocation + "/in.csv"), new File(directoryLocation + "/output.xml"))
        WSSimulator.shutdown()
    }
}