package weather

import com.sun.xml.internal.ws.util.xml.CDATA
import org.apache.camel.Body
import org.apache.commons.io.FileUtils
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import spock.lang.Specification
import wssimulator.WSSimulator

import javax.xml.stream.Location
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit
import static weather.TestingUtils.copyFromTo
import static weather.TestingUtils.deleteFiles
import static weather.TestingUtils.waitFor

/**
 * Simple test to show a test world integration test - this is an end to end test that
 * will place a csv file onto a directory and wait for sample to do its thing.
 * Note - this is backed by a junit4runner so can be simple executed within an IDE
 * or by using the gradle script
 * ./gradlew.bat -Dtest.single=CallRealWebServiceIntegrationTestSpecification test
 */
        @ContextConfiguration(classes = Application.class)
        @TestPropertySource(locations = "classpath:test-integration.properties")
//uses real world values
        class CallRealWebServiceIntegrationTestSpecification extends Specification {

            public static final String directoryLocation = "/tmp"

            def "CSV Routing with a real service"() {
                given: "put the CSV into the directory that CAMEL is listening to"
                copyFromTo("/input/weather/cities.csv", new File(directoryLocation + "/cities.csv"))

                and: "start WSSimulator"
                WSSimulator.setPort(8866)
                WSSimulator.addSimulation(getClass().getResource("/ws/weather/sample-get-weather-response.yml").getFile() as File);

                when:
                waitFor(5, TimeUnit.SECONDS) //let camel do its thing.

                then: "read the written values back into this test so we can output them to a print stream (or going further by validating the response)"
                String output = FileUtils.readFileToString(new File(directoryLocation + "/output.xml"), Charset.defaultCharset())
                String cities = FileUtils.readFileToString(new File(directoryLocation + "/cities.csv"), Charset.defaultCharset())

                //compare String cities with expected values
                cities == "Belfast,United Kingdom"
                //return  String cities
                println "Expeced cities used: \n$cities\n"


                //test that output returns the expected values for tags
                output.contains("<Time>Jul 05, 2016 - 05:50 AM EDT / 2016.07.05 0950 UTC</Time>")
                println("output.xml contains correct time response")

                output.contains("<Wind> from the WNW (300 degrees) at 8 MPH (7 KT) (direction variable):0</Wind>")
                println("output.xml contains correct wind response")

                output.contains("<Visibility> greater than 7 mile(s):0</Visibility>")
                println("output.xml contains correct visibility response")

                output.contains("<SkyConditions> mostly cloudy</SkyConditions>")
                println("output.xml contains correct skyconditions response")

                output.contains("<Temperature> 57 F (14 C)</Temperature>")
                println("output.xml contains correct temperature response")

                output.contains("<DewPoint> 48 F (9 C)</DewPoint>")
                println("output.xml contains correct dew point response")

                output.contains("<RelativeHumidity> 71%</RelativeHumidity>")
                println("output.xml contains correct relative humidity response")

                output.contains("<Pressure> 30.09 in. Hg (1019 hPa)</Pressure>")
                println("output.xml contains correct Pressure response")

                output.contains("<Status>Success</Status>")
                println("output.xml contains correct status response")
                println("output.xml contains all the expected values")

                output.contains("<GetWeatherResponse xmlns=\"http://www.webserviceX.NET\">")

                println "output:-------\n$output\n-------"

                true
                cleanup: "delete all files"
                deleteFiles(new File(directoryLocation + "/in.csv"), new File(directoryLocation + "/output.xml"))
                WSSimulator.shutdown()
            }
        }